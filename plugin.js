var app = null;

/* TO BE EXPORTED TO DIFFERENT FILE  */
function CreateTaskTypeDialog(params, createDom = true) {
  jsa.Modal.call(this, params, false);

  //parameters
  this.containerStyle = ["jsa-modal-container"];
  this.task = null;
  this.taskName = "UnnamedTask";
  this.defaultLibraryId = null;
  this.defaultLibraryName = "Undefined";

  //copy paramters
  jsa.CopyParams(this, params);

  if (createDom) {
    this.CreateDom();
  }

  return this;
}

CreateTaskTypeDialog.prototype = Object.create(jsa.Modal.prototype);

CreateTaskTypeDialog.prototype.CreateDom = function () {
  //call the underlying dialog Dom creation
  jsa.Modal.prototype.CreateDom.call(this);

  this.description = new jsa.CustomUiElement({
    elementType: "p",
    style: ["description"],
    content: "Please specify the parameters for TaskType creation.",
  });

  this.AddChild(this.description);
  this.ctrlTable = new jsa.Table({
    style: ["action-arguments-table"],
  });
  this.AddChild(this.ctrlTable);

  // Library control field
  let libCtrlRow = new jsa.TableRow();
  let libNameCol = new jsa.TableCol({
    style: ["name"],
    content: "Library",
  });
  let libCtrlCol = new jsa.TableCol();
  libCtrlRow.AddChild(libNameCol);
  libCtrlRow.AddChild(libCtrlCol);

  let libraryCtrlParams = {
    // style: ['form-group','no-margin'],
    // ctrlStyle: ['form-control','form-control-sm'],
    value: null,
    readonly: false,
    placeholder: "OAAM Library",
    upperBound: 1,
    lowerBound: 0,
    typeName: "Library",
    containment: false,
    packageNs: "http://www.oaam.de/oaam/model/v160/library",
    ecoreSync: $app.ecoreSync,
    data: null,
    onChangeCallback: function (e) {
      return true;
    },
    inputTransferFunction: function (value) {
      let valueStr = value;
      return valueStr;
    },
    outputTransferFunction: function (valueStr) {
      let value = "";
      let valueSegments = valueStr.split(";");
      let objectIds = [];
      for (let i = 0; i < valueSegments.length; i++) {
        let valueSegment = valueSegments[i];
        let idStrs = valueSegment.match(/#\d+/g);
        if (idStrs) {
          //neglect any illegal segments, which does not contain an object id.
          let idStr = idStrs[0]; //array must have exactly one element since it was validated before
          objectIds.push(Number.parseInt(idStr.substring(1))); //omit the # at the beginning
        }
      }
      if (this.upperBound == 1) {
        if (objectIds.length == 0) {
          //value = '';
          value = null;
        } else {
          let objectId = objectIds[0]; //always take the first one
          //value = '#'+objectId;
          value = new eoq2.Qry().Obj(objectId);
        }
      } else {
        //upper bound > 1
        value = [];
        for (let i = 0; i < objectIds.length; i++) {
          value.push(new eoq2.Qry().Obj(objectIds[i]));
        }
        // if(objectIds.length==0) {
        //     //value = '[]';

        // } else {
        //     //value = '[#'+objectIds.join(',#')+']';
        //     value = new eoq2.Qry().Obj(objectId);
        // }
      }
      return value;
    },
  };
  this.libraryCtrl = new ECORESYNC_UI.EReferenceCtrl(libraryCtrlParams);
  this.libraryCtrl.SetRawValue(
    "'" + this.defaultLibraryName + "' [#" + this.defaultLibraryId + "]",
    true,
  );
  libCtrlCol.AddChild(this.libraryCtrl);
  this.ctrlTable.AddChild(libCtrlRow);

  // Task Name Ctrl field

  let taskTypeNameCtrlRow = new jsa.TableRow();
  let taskTypeNameCol = new jsa.TableCol({
    style: ["name"],
    content: "TaskType Name",
  });
  let taskTypeNameCtrlCol = new jsa.TableCol();

  taskTypeNameCtrlRow.AddChild(taskTypeNameCol);
  taskTypeNameCtrlRow.AddChild(taskTypeNameCtrlCol);

  let taskTypeCtrlParams = {
    // style: ['form-group','no-margin'],
    // ctrlStyle: ['form-control','form-control-sm'],
    value: this.taskName,
    readonly: false,
    placeholder: "String",
    data: { eObject: null, featureName: "" },
    onChangeCallback: function (e) {
      return true;
    },
  };

  this.taskTypeNameCtrl = new ECORESYNC_UI.EStringCtrl(taskTypeCtrlParams);
  taskTypeNameCtrlCol.AddChild(this.taskTypeNameCtrl);
  this.ctrlTable.AddChild(taskTypeNameCtrlRow);

  // Add to Task option ctrl

  let addTaskTypeCtrlRow = new jsa.TableRow();
  let addTaskTypeNameCol = new jsa.TableCol({
    style: ["name"],
    content: "Add TaskType to Task?",
  });

  let addTaskTypeCtrlCol = new jsa.TableCol();
  addTaskTypeCtrlRow.AddChild(addTaskTypeNameCol);
  addTaskTypeCtrlRow.AddChild(addTaskTypeCtrlCol);

  let addTaskTypeCtrlParams = {
    value: true,
    readonly: false,
    placeholder: "Bool",
    data: { eObject: null, featureName: "" },
    onChangeCallback: function (e) {
      return true;
    },
  };

  this.addTaskTypeCtrl = new ECORESYNC_UI.EBoolCtrl(addTaskTypeCtrlParams);
  addTaskTypeCtrlCol.AddChild(this.addTaskTypeCtrl);
  this.ctrlTable.AddChild(addTaskTypeCtrlRow);

  return this;
};

async function ShowCreateTaskTypeDialog(task) {
  let taskName = await ecoreSync.get(task, "name");
  var dialog = new CreateTaskTypeDialog(
    {
      style: ["jsa-modal", "actions-dialog"],
      name: "Create TaskType",
      task: task,
      taskName: taskName,
      defaultLibraryId: ecoreSync.rlookup(
        await ecoreSync.remoteExec(
          new eoq2.Get(
            new eoq2.Obj(ecoreSync.rlookup(task))
              .Met("ALLPARENTS")
              .Sel(QRY.Met("CLASSNAME").Equ("Architecture"))
              .Ino("Library")
              .Idx("FLATTEN")
              .Trm(QRY.Met("SIZE").Equ(0), null)
              .Idx(0),
          ),
          true,
        ),
      ),
      defaultLibraryName: await ecoreSync.remoteExec(
        new eoq2.Get(
          new eoq2.Obj(ecoreSync.rlookup(task))
            .Met("ALLPARENTS")
            .Sel(QRY.Met("CLASSNAME").Equ("Architecture"))
            .Ino("Library")
            .Idx("FLATTEN")
            .Trm(QRY.Met("SIZE").Equ(0), null)
            .Idx(0)
            .Pth("name"),
        ),
      ),
    },
    false,
  );

  //replace the buttons
  dialog.buttons = {
    cancel: {
      name: "Cancel",
      startEnabled: true,
      data: dialog,
      onClickCallback: function (event) {
        this.data.Dissolve(); //Destroy the dialog
      },
    },
    run: {
      name: "Create TaskType",
      startEnabled: true,
      data: dialog,
      onClickCallback: function (event) {
        let addTypeToTask = this.data.addTaskTypeCtrl.GetValue();
        let selectedLibrary = this.data.libraryCtrl.GetValue();
        let selectedTaskTypeName = this.data.taskTypeNameCtrl.GetValue();
        ecoreSync.remoteExec(
          new eoq2.Cal("oaam/types/CreateTaskTypeFromTask", [
            selectedLibrary.v[0],
            ecoreSync.utils.encode(task),
            selectedTaskTypeName,
            addTypeToTask,
          ]),
        );
        this.data.Dissolve();
      },
    },
  };
  dialog.CreateDom();

  //Show the dialog
  app.AddChild(dialog);
}

/* END OF TO BE EXPORTED SECTION */

/* TO BE EXPORTED TO DIFFERENT FILE  */
/* NOTE: CAN BE UNIFIED WITH TASKTYPE DIALOG TO A MORE GENERIC APPROACH */
function CreateDeviceTypeDialog(params, createDom = true) {
  jsa.Modal.call(this, params, false);

  //parameters
  this.containerStyle = ["jsa-modal-container"];
  this.device = null;
  this.deviceName = "UnnamedDevice";
  this.defaultLibraryId = null;
  this.defaultLibraryName = "Undefined";

  //copy paramters
  jsa.CopyParams(this, params);

  if (createDom) {
    this.CreateDom();
  }

  return this;
}

CreateDeviceTypeDialog.prototype = Object.create(jsa.Modal.prototype);

CreateDeviceTypeDialog.prototype.CreateDom = function () {
  //call the underlying dialog Dom creation
  jsa.Modal.prototype.CreateDom.call(this);

  this.description = new jsa.CustomUiElement({
    elementType: "p",
    style: ["description"],
    content: "Please specify the parameters for DeviceType creation.",
  });

  this.AddChild(this.description);
  this.ctrlTable = new jsa.Table({
    style: ["action-arguments-table"],
  });
  this.AddChild(this.ctrlTable);

  // Library control field
  let libCtrlRow = new jsa.TableRow();
  let libNameCol = new jsa.TableCol({
    style: ["name"],
    content: "Library",
  });
  let libCtrlCol = new jsa.TableCol();
  libCtrlRow.AddChild(libNameCol);
  libCtrlRow.AddChild(libCtrlCol);

  let libraryCtrlParams = {
    // style: ['form-group','no-margin'],
    // ctrlStyle: ['form-control','form-control-sm'],
    value: null,
    readonly: false,
    placeholder: "OAAM Library",
    upperBound: 1,
    lowerBound: 0,
    typeName: "Library",
    containment: false,
    packageNs: "http://www.oaam.de/oaam/model/v160/library",
    ecoreSync: $app.ecoreSync,
    data: null,
    onChangeCallback: function (e) {
      return true;
    },
    inputTransferFunction: function (value) {
      let valueStr = value;
      return valueStr;
    },
    outputTransferFunction: function (valueStr) {
      let value = "";
      let valueSegments = valueStr.split(";");
      let objectIds = [];
      for (let i = 0; i < valueSegments.length; i++) {
        let valueSegment = valueSegments[i];
        let idStrs = valueSegment.match(/#\d+/g);
        if (idStrs) {
          //neglect any illegal segments, which does not contain an object id.
          let idStr = idStrs[0]; //array must have exactly one element since it was validated before
          objectIds.push(Number.parseInt(idStr.substring(1))); //omit the # at the beginning
        }
      }
      if (this.upperBound == 1) {
        if (objectIds.length == 0) {
          //value = '';
          value = null;
        } else {
          let objectId = objectIds[0]; //always take the first one
          //value = '#'+objectId;
          value = new eoq2.Qry().Obj(objectId);
        }
      } else {
        //upper bound > 1
        value = [];
        for (let i = 0; i < objectIds.length; i++) {
          value.push(new eoq2.Qry().Obj(objectIds[i]));
        }
        // if(objectIds.length==0) {
        //     //value = '[]';

        // } else {
        //     //value = '[#'+objectIds.join(',#')+']';
        //     value = new eoq2.Qry().Obj(objectId);
        // }
      }
      return value;
    },
  };
  this.libraryCtrl = new ECORESYNC_UI.EReferenceCtrl(libraryCtrlParams);
  this.libraryCtrl.SetRawValue(
    "'" + this.defaultLibraryName + "' [#" + this.defaultLibraryId + "]",
    true,
  );
  libCtrlCol.AddChild(this.libraryCtrl);
  this.ctrlTable.AddChild(libCtrlRow);

  // device Name Ctrl field

  let DeviceTypeNameCtrlRow = new jsa.TableRow();
  let DeviceTypeNameCol = new jsa.TableCol({
    style: ["name"],
    content: "DeviceType Name",
  });
  let DeviceTypeNameCtrlCol = new jsa.TableCol();

  DeviceTypeNameCtrlRow.AddChild(DeviceTypeNameCol);
  DeviceTypeNameCtrlRow.AddChild(DeviceTypeNameCtrlCol);

  let DeviceTypeCtrlParams = {
    // style: ['form-group','no-margin'],
    // ctrlStyle: ['form-control','form-control-sm'],
    value: this.deviceName,
    readonly: false,
    placeholder: "String",
    data: { eObject: null, featureName: "" },
    onChangeCallback: function (e) {
      return true;
    },
  };

  this.DeviceTypeNameCtrl = new ECORESYNC_UI.EStringCtrl(DeviceTypeCtrlParams);
  DeviceTypeNameCtrlCol.AddChild(this.DeviceTypeNameCtrl);
  this.ctrlTable.AddChild(DeviceTypeNameCtrlRow);

  // Add to device option ctrl

  let addDeviceTypeCtrlRow = new jsa.TableRow();
  let addDeviceTypeNameCol = new jsa.TableCol({
    style: ["name"],
    content: "Add DeviceType to device?",
  });

  let addDeviceTypeCtrlCol = new jsa.TableCol();
  addDeviceTypeCtrlRow.AddChild(addDeviceTypeNameCol);
  addDeviceTypeCtrlRow.AddChild(addDeviceTypeCtrlCol);

  let addDeviceTypeCtrlParams = {
    value: true,
    readonly: false,
    placeholder: "Bool",
    data: { eObject: null, featureName: "" },
    onChangeCallback: function (e) {
      return true;
    },
  };

  this.addDeviceTypeCtrl = new ECORESYNC_UI.EBoolCtrl(addDeviceTypeCtrlParams);
  addDeviceTypeCtrlCol.AddChild(this.addDeviceTypeCtrl);
  this.ctrlTable.AddChild(addDeviceTypeCtrlRow);

  return this;
};

async function ShowCreateDeviceTypeDialog(device) {
  let deviceName = await ecoreSync.get(device, "name");
  var dialog = new CreateDeviceTypeDialog(
    {
      style: ["jsa-modal", "actions-dialog"],
      name: "Create DeviceType",
      device: device,
      deviceName: deviceName,
      defaultLibraryId: ecoreSync.rlookup(
        await ecoreSync.remoteExec(
          new eoq2.Get(
            new eoq2.Obj(ecoreSync.rlookup(device))
              .Met("ALLPARENTS")
              .Sel(QRY.Met("CLASSNAME").Equ("Architecture"))
              .Ino("Library")
              .Idx("FLATTEN")
              .Trm(QRY.Met("SIZE").Equ(0), null)
              .Idx(0),
          ),
          true,
        ),
      ),
      defaultLibraryName: await ecoreSync.remoteExec(
        new eoq2.Get(
          new eoq2.Obj(ecoreSync.rlookup(device))
            .Met("ALLPARENTS")
            .Sel(QRY.Met("CLASSNAME").Equ("Architecture"))
            .Ino("Library")
            .Idx("FLATTEN")
            .Trm(QRY.Met("SIZE").Equ(0), null)
            .Idx(0)
            .Pth("name"),
        ),
      ),
    },
    false,
  );

  //replace the buttons
  dialog.buttons = {
    cancel: {
      name: "Cancel",
      startEnabled: true,
      data: dialog,
      onClickCallback: function (event) {
        this.data.Dissolve(); //Destroy the dialog
      },
    },
    run: {
      name: "Create DeviceType",
      startEnabled: true,
      data: dialog,
      onClickCallback: function (event) {
        let addTypeToDevice = this.data.addDeviceTypeCtrl.GetValue();
        let selectedLibrary = this.data.libraryCtrl.GetValue();
        let selectedDeviceTypeName = this.data.DeviceTypeNameCtrl.GetValue();
        ecoreSync.remoteExec(
          new eoq2.Cal("oaam/types/CreateDeviceTypeFromDevice", [
            selectedLibrary.v[0],
            ecoreSync.utils.encode(device),
            selectedDeviceTypeName,
            addTypeToDevice,
          ]),
        );
        this.data.Dissolve();
      },
    },
  };
  dialog.CreateDom();

  //Show the dialog
  app.AddChild(dialog);
}

/* END OF TO BE EXPORTED SECTION */

export function init(pluginAPI) {
  var contextMenu = pluginAPI.require("contextMenu");
  var eoqUtils = pluginAPI.require("contextMenu.eoq").util;
  app = pluginAPI.getGlobal("app");

  //register OAAM model resource with EOQ workspace
  class OAAMWorkspaceContextMenuProvider extends pluginAPI.getInterface("ecoreTreeView.menus") {
    constructor() {
      super();
    }

    isApplicableToNode(node) {
      if (
        node.data.eObject.eClass.eContainer
          .get("nsURI")
          .includes("http://www.eoq.de/workspacemdbmodel/v1.0")
      ) {
        switch (node.data.eObject.eClass.get("name")) {
          case "Workspace":
            return true;
          case "Directory":
            return true;
          case "ModelResource":
            return false;
          default:
            return false;
        }
      }
      return false;
    }

    getContextMenu(node) {
      var cMenu = false;

      if (
        node.data.eObject.eClass.eContainer
          .get("nsURI")
          .includes("http://www.eoq.de/workspacemdbmodel/v1.0")
      ) {
        switch (node.data.eObject.eClass.get("name")) {
          case "Workspace":
            cMenu = contextMenu.createContextMenu("oaam-eoq-context-menu", "Eoq Context Menu", 1);

            var creationMenu = contextMenu.createContextMenu(
              "oaam-context-menu-sub-create",
              "New",
              1,
              "add",
            );
            cMenu.addSubMenu("create", creationMenu);

            creationMenu.addNewEntry(
              "create-oaam",
              "OAAM Model",
              function () {
                eoqUtils.createModelResource(
                  node.data.eObject,
                  "newModel",
                  "oaam",
                  "http://www.oaam.de/oaam/model/v160",
                  "Architecture",
                );
              },
              "add",
            );

            break;
          case "Directory":
            cMenu = contextMenu.createContextMenu("oaam-eoq-context-menu", "Eoq Context Menu", 1);

            var creationMenu = contextMenu.createContextMenu(
              "oaam-eoq-context-menu-sub-create",
              "New",
              1,
              "add",
            );
            cMenu.addSubMenu("create", creationMenu);

            creationMenu.addNewEntry(
              "create-oaam",
              "OAAM Model",
              function () {
                eoqUtils.createModelResource(
                  node.data.eObject,
                  "newModel",
                  "oaam",
                  "http://www.oaam.de/oaam/model/v160",
                  "Architecture",
                );
              },
              "add",
            );

            break;
          default:
            return false;
        }
      }

      return cMenu;
    }
  }

  //register OAAM model graphobjects
  class OAAMGraphObject extends pluginAPI.getInterface("editor.menus") {
    constructor() {
      super();
    }
    isApplicableToTarget(target) {
      return target.isVertex;
    }
    getContextMenu(target) {
      var cMenu = null;
      if (target.isGraphObject) {
        if (target.isVertex && target.eObject && target.eObject.eClass.get("name") == "Device") {
          var cMenu = contextMenu.createContextMenu("oaam-context-menu", "OAAM Context Menu", 99);
          var setTypeMenu = contextMenu.createContextMenu(
            "oaam-eoq-context-menu-device-set-type",
            "Set Device Type",
            1,
            "edit",
          );
          cMenu.addSubMenu("setType", setTypeMenu);
          setTypeMenu.addEntryProvider("device-types", async function () {
            var entries = {};
            var cmd = new eoq2.Cmp();
            var mdlRoot = await ecoreSync.utils.getModelRoot(target.eObject);
            cmd.Get(QRY.Obj(ecoreSync.rlookup(mdlRoot)).Cls("DeviceType"));
            cmd.Get(QRY.His(-1).Pth("name"));
            var devTypes = await ecoreSync.remoteExec(cmd, true);

            devTypes[1].forEach(function (typeName, i) {
              entries["set_type_" + typeName.toLowerCase()] = {
                type: "entry",
                value: contextMenu.createContextMenuEntry(
                  typeName,
                  async () => {
                    return ecoreSync.set(target.eObject, "type", devTypes[0][i]);
                  },
                  "add",
                ),
              };
            });

            var remoteActions = await ecoreSync.remoteExec(new eoq2.Gaa());

            // Check if the create task type from task action is available and add corresponding entry
            if (
              remoteActions.some((action) => {
                return action[0] == "oaam/types/CreateDeviceTypeFromDevice";
              })
            ) {
              entries["create_device_type"] = {
                type: "entry",
                value: contextMenu.createContextMenuEntry(
                  "Create from Device",
                  async () => {
                    ShowCreateDeviceTypeDialog(target.eObject);
                  },
                  "edit",
                ),
              };
            }

            return entries;
          });
        }

        if (target.isVertex && target.eObject && target.eObject.eClass.get("name") == "Task") {
          var cMenu = contextMenu.createContextMenu("oaam-context-menu", "OAAM Context Menu", 99);
          var setTypeMenu = contextMenu.createContextMenu(
            "oaam-eoq-context-menu-device-set-type",
            "Set Task Type",
            1,
            "edit",
          );
          cMenu.addSubMenu("setType", setTypeMenu);
          setTypeMenu.addEntryProvider("task-types", async function () {
            var entries = {};
            var cmd = new eoq2.Cmp();
            var mdlRoot = await ecoreSync.utils.getModelRoot(target.eObject);
            cmd.Get(QRY.Obj(ecoreSync.rlookup(mdlRoot)).Cls("TaskType"));
            cmd.Get(QRY.His(-1).Pth("name"));
            var taskTypes = await ecoreSync.remoteExec(cmd, true);

            taskTypes[1].forEach(function (typeName, i) {
              entries["set_type_" + typeName.toLowerCase()] = {
                type: "entry",
                value: contextMenu.createContextMenuEntry(
                  typeName,
                  async () => {
                    return ecoreSync.set(target.eObject, "type", taskTypes[0][i]);
                  },
                  "add",
                ),
              };
            });

            var remoteActions = await ecoreSync.remoteExec(new eoq2.Gaa());

            // Check if the create task type from task action is available and add corresponding entry
            if (
              remoteActions.some((action) => {
                return action[0] == "oaam/types/CreateTaskTypeFromTask";
              })
            ) {
              entries["create_task_type"] = {
                type: "entry",
                value: contextMenu.createContextMenuEntry(
                  "Create from Task",
                  async () => {
                    ShowCreateTaskTypeDialog(target.eObject);
                  },
                  "edit",
                ),
              };
            }

            return entries;
          });
        }

        if (
          target.isVertex &&
          target.eObject &&
          target.eObject.eClass.get("name") == "Io" &&
          target.eObject.eContainer.get("type") != null
        ) {
          var cMenu = contextMenu.createContextMenu("oaam-context-menu", "OAAM Context Menu", 99);
          var setDeclMenu = contextMenu.createContextMenu(
            "oaam-eoq-context-menu-device-set-type",
            "Set IO declaration",
            1,
            "edit",
          );
          cMenu.addSubMenu("setDeclaration", setDeclMenu);
          setDeclMenu.addEntryProvider("io-declarations", async function () {
            var entries = {};
            var cmd = new eoq2.Cmp();
            cmd.Get(
              QRY.Obj(ecoreSync.rlookup(target.eObject.eContainer.get("type"))).Cls(
                "IoDeclaration",
              ),
            );
            cmd.Get(QRY.His(-1).Pth("name"));
            var declarations = await ecoreSync.remoteExec(cmd, true);
            declarations[1].forEach(function (declarationName, i) {
              entries["set_decl_" + declarationName.toLowerCase()] = {
                type: "entry",
                value: contextMenu.createContextMenuEntry(
                  declarationName,
                  async () => {
                    return ecoreSync.set(target.eObject, "declaration", declarations[0][i]);
                  },
                  "add",
                ),
              };
            });

            return entries;
          });
        }
      }
      return cMenu;
    }
  }

  pluginAPI.implement("ecoreTreeView.menus", new OAAMWorkspaceContextMenuProvider());
  pluginAPI.implement("editor.menus", new OAAMGraphObject());
  return Promise.resolve();
}

export var meta = {
  id: "contextMenu.oaam",
  description: "Context menu for OAAM",
  author: "Matthias Brunner",
  version: "0.0.1",
  requires: ["contextMenu", "contextMenu.eoq", "plugin.ecoreTreeView"],
};
